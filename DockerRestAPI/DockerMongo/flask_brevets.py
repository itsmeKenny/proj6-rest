"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import os
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
myli = []
client = MongoClient("db", 27017)
db = client.tododb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.errorhandler(403)
def no_datatodisplay(error):
    app.logger.debug("No data to display. Try again")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('nodatadisplay.html'), 403

@app.errorhandler(403)
def no_datagiven(error):
    app.logger.debug("No data given. Try again")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('nodatagive.html'), 403


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############

#This function will be the display function
#Once called we will display the calc page with items
@app.route('/mytodo')
def mytodo():
    _items = db.tododb.find()
    items = [item for item in _items]
    if len(items) == 0:
        return no_datatodisplay(403)
    return render_template('mytodo.html', items=items)

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    mi = request.args.get('miles')
    loc = request.args.get('location')
    print("location at :",loc)
    dist = request.args.get('distance')
    dist = float(dist)
    begin_date = request.args.get('begin_date')
    begin_time = request.args.get('begin_time')
    strdate = (begin_date + "T" + begin_time+"-08:00")
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, dist, strdate)
    close_time = acp_times.close_time(km, dist, strdate)
    item_doc = {'open': open_time, 
    'close': close_time, 
    'distance': dist,
    'begind': begin_date,
    'begint': begin_time,
    'mi': mi,
    'km':km,
    'loc':loc
    }
    myli.append(item_doc) #Adds information to myli to be transfered to submittimes

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route("/submit_times", methods=['POST'])
def submit_times():
    if len(myli) == 0:
        return no_datagiven(403)
    mydict = {} #dict to be sent to db
    for i in myli: #loading information into dict from myli
        mydict['km'] = i['km']
        mydict['mi'] = i['mi']
        mydict['loc'] = i['loc']
        mydict['distance'] = i['distance']
        mydict['begind'] = i['begind']
        mydict['begint'] = i['begint']
        mydict['close'] = i['close']
        mydict['open'] = i['open'] 
        db.tododb.insert_one(mydict)
        mydict = {} #Clear mydict for next set of data
    del myli[:] #Clear myli for next input
    return flask.render_template('calc.html')


app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
