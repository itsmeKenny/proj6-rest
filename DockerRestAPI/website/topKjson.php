<html>
    <head>
        <title>CIS 322 REST-api Brevet Data</title>
    </head>
    <body>
        <h1>List of top 5 open times JSON</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly/json?top=5');
            $obj = json_decode($json);
	          $alltimes = $obj->brevets;
              foreach ($alltimes as $l) {
                $open = $l->open;
                $mi = $l->mi;
                $loc = $l->loc;
                $km = $l->km;
                $begint = $l->begint;
                $begind = $l->begind;
                $distance = $l->distance;
                echo "<li>Distance:$distance, KM:$km, Miles:$mi, Location:$loc, Begin Date: $begind, Begin Time: $begint, Open: $open</li>";
            }
            ?>
        </ul>
    </body>
    <body>
        <h1>List of top 4 close times JSON</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly/json?top=4');
            $obj = json_decode($json);
	          $alltimes = $obj->brevets;
              foreach ($alltimes as $l) {
                $close = $l->close;
                $mi = $l->mi;
                $loc = $l->loc;
                $km = $l->km;
                $begint = $l->begint;
                $begind = $l->begind;
                $distance = $l->distance;
                echo "<li>Distance:$distance, KM:$km, Miles:$mi, Location:$loc, Begin Date: $begind, Begin Time: $begint, Close: $close</li>";
            }
            ?>
        </ul>
    </body>
</html>