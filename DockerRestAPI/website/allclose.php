<html>
    <head>
        <title>CIS 322 REST-api Brevet Data</title>
    </head>
    <body>
        <h1>List of close times</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly');
            $obj = json_decode($json);
	          $alltimes = $obj->brevets;
              foreach ($alltimes as $l) {
                $mi = $l->mi;
                $loc = $l->loc;
                $close = $l->close;
                $km = $l->km;
                $begint = $l->begint;
                $begind = $l->begind;
                $distance = $l->distance;
                echo "<li>Distance:$distance, KM:$km, Miles:$mi, Location:$loc, Begin Date: $begind, Begin Time: $begint, Close: $close</li>";
            }
            ?>
        </ul>
    </body>
</html>