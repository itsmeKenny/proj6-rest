<html>
    <head>
        <title>CIS 322 REST-api Brevet Data</title>
    </head>
    <body>
        <h1>List of all times</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
	          $alltimes = $obj->brevets;
            foreach ($alltimes as $l) {
                $open = $l->open;
                $mi = $l->mi;
                $loc = $l->loc;
                $close = $l->close;
                $km = $l->km;
                $begint = $l->begint;
                $begind = $l->begind;
                $distance = $l->distance;
                echo "<li>Distance:$distance, KM:$km, Miles:$mi, Location:$loc, Begin Date: $begind, Begin Time: $begint, Open: $open, Close: $close</li>";
            }
            ?>
        </ul>
    </body>
    <body>
        <h1>List of open times</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly');
            $obj = json_decode($json);
	          $alltimes = $obj->brevets;
              foreach ($alltimes as $l) {
                $open = $l->open;
                $mi = $l->mi;
                $loc = $l->loc;
                $km = $l->km;
                $begint = $l->begint;
                $begind = $l->begind;
                $distance = $l->distance;
                echo "<li>Distance:$distance, KM:$km, Miles:$mi, Location:$loc, Begin Date: $begind, Begin Time: $begint, Open: $open</li>";
            }
            ?>
        </ul>
    </body>

    <body>
        <h1>List of close times</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly');
            $obj = json_decode($json);
	          $alltimes = $obj->brevets;
              foreach ($alltimes as $l) {
                $mi = $l->mi;
                $loc = $l->loc;
                $close = $l->close;
                $km = $l->km;
                $begint = $l->begint;
                $begind = $l->begind;
                $distance = $l->distance;
                echo "<li>Distance:$distance, KM:$km, Miles:$mi, Location:$loc, Begin Date: $begind, Begin Time: $begint, Close: $close</li>";
            }
            ?>
        </ul>
    </body>

    <body>
        <h1>List of all times in JSON</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll/json');
            $obj = json_decode($json);
	          $alltimes = $obj->brevets;
            echo $json;
            ?>
        </ul>
    </body>

    <body>
        <h1>List of Open times in JSON</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly/json');
            $obj = json_decode($json);
	          $alltimes = $obj->brevets;
            echo $json;
            ?>
        </ul>
    </body>

    <body>
        <h1>List of Close times in JSON</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly/json');
            $obj = json_decode($json);
	          $alltimes = $obj->brevets;
            echo $json;
            ?>
        </ul>
    </body>

    <body>
        <h1>List of All times in CSV</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll/csv');
            echo $json;
            ?>
        </ul>
    </body>

    <body>
        <h1>List of Open times in CSV</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly/csv');
            echo $json;
            ?>
        </ul>
    </body>

    <body>
        <h1>List of Close times in CSV</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly/csv');
            echo $json;
            ?>
        </ul>
    </body>

    <body>
        <h1>List of top 5 open times JSON</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly/json?top=5');
            $obj = json_decode($json);
	          $alltimes = $obj->brevets;
              foreach ($alltimes as $l) {
                $open = $l->open;
                $mi = $l->mi;
                $loc = $l->loc;
                $km = $l->km;
                $begint = $l->begint;
                $begind = $l->begind;
                $distance = $l->distance;
                echo "<li>Distance:$distance, KM:$km, Miles:$mi, Location:$loc, Begin Date: $begind, Begin Time: $begint, Open: $open</li>";
            }
            ?>
        </ul>
    </body>

    <body>
        <h1>List of top 4 close times JSON</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly/json?top=4');
            $obj = json_decode($json);
	          $alltimes = $obj->brevets;
              foreach ($alltimes as $l) {
                $close = $l->close;
                $mi = $l->mi;
                $loc = $l->loc;
                $km = $l->km;
                $begint = $l->begint;
                $begind = $l->begind;
                $distance = $l->distance;
                echo "<li>Distance:$distance, KM:$km, Miles:$mi, Location:$loc, Begin Date: $begind, Begin Time: $begint, Close: $close</li>";
            }
            ?>
        </ul>
    </body>

    <body>
        <h1>List of top 3 open times in CSV</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly/csv?top=3');
            echo $json;
            ?>
        </ul>
    </body>
    <body>
        <h1>List of top 6 close times in CSV</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly/csv?top=6');
            echo $json;
            ?>
        </ul>
    </body>
</html>
