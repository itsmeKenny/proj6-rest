<html>
    <head>
        <title>CIS 322 REST-api Brevet Data</title>
    </head>
    <body>
        <h1>List of open times</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly');
            $obj = json_decode($json);
	          $alltimes = $obj->brevets;
              foreach ($alltimes as $l) {
                $open = $l->open;
                $mi = $l->mi;
                $loc = $l->loc;
                $km = $l->km;
                $begint = $l->begint;
                $begind = $l->begind;
                $distance = $l->distance;
                echo "<li>Distance:$distance, KM:$km, Miles:$mi, Location:$loc, Begin Date: $begind, Begin Time: $begint, Open: $open</li>";
            }
            ?>
        </ul>
    </body>
</html>