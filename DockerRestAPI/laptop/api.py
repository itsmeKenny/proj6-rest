# Laptop Service

from flask import Flask, make_response
import flask
import pymongo
from flask_restful import Resource, Api
import csv
import re
from pymongo import MongoClient

# Instantiate the app
#feeling good
app = Flask(__name__)
api = Api(app)
client = MongoClient("db", 27017)
db = client.tododb

class listAll(Resource): #Works as intended
    def get(self):
        _items = db.tododb.find({},{ "_id": 0 })
        items = [item for item in _items]
        rslt = {'brevets': items}
        return rslt
api.add_resource(listAll, '/listAll')

class listOpenOnly(Resource): #Works as intended
    def get(self):
        _items = db.tododb.find({},{ "_id": 0, "close":0})
        items = [item for item in _items]
        rslt = {'brevets': items}
        return rslt
api.add_resource(listOpenOnly, '/listOpenOnly')

class listCloseOnly(Resource): #Works as intended
    def get(self):
        _items = db.tododb.find({},{ "_id": 0 ,"open":0})
        items = [item for item in _items]
        rslt = {'brevets': items}
        return rslt
api.add_resource(listCloseOnly, '/listCloseOnly')

###############################################

class listAlljson(Resource): #Works as intended
    def get(self):
        _items = db.tododb.find({},{ "_id": 0 })
        items = [item for item in _items]
        rslt = {'brevets': items}
        return rslt
api.add_resource(listAlljson, '/listAll/json')

class listClosejson(Resource): #Works as intended
    def get(self):
        items =[]
        k = 0
        count = 0
        _items = db.tododb.find({},{ "_id": 0, "open":0})
        if (flask.request.args.get('top')):
            k = int(flask.request.args.get('top'))
        if k > 0:
            _items = db.tododb.find({},{ "_id": 0, "open":0}).sort('open',pymongo.ASCENDING)
            for item in _items:
                items.append(item)
                count += 1
                if k == count:
                    break
        else:
            items = [item for item in _items]
        rslt = {'brevets': items}
        return rslt
api.add_resource(listClosejson, '/listCloseOnly/json')

class listOpenjson(Resource): #Works as intended
    def get(self):
        items =[]
        k = 0
        count = 0
        _items = db.tododb.find({},{ "_id": 0, "close":0})
        if (flask.request.args.get('top')):
            k = int(flask.request.args.get('top'))
        if k > 0:
            _items = db.tododb.find({},{ "_id": 0, "close":0}).sort('open',pymongo.ASCENDING)
            for item in _items:
                items.append(item)
                count += 1
                if k == count:
                    break
        else:
            items = [item for item in _items]
        rslt = {'brevets': items}
        return rslt
api.add_resource(listOpenjson, '/listOpenOnly/json')

###############################################

class listAllcvs(Resource): #Works as intended
    def get(self):
        _items = db.tododb.find({},{ "_id": 0 })
        items = [item for item in _items]
        item = str(re.sub('\[','',str(items)))
        item = str(re.sub(']','',str(item)))
        item = str(re.sub('}','',str(item)))
        item = str(re.sub('{','',str(item)))
        response = make_response(item)
        return response
api.add_resource(listAllcvs, '/listAll/csv')

class listOpencsv(Resource): #Works as intended
    def get(self):
        items =[]
        k = 0
        count = 0
        _items = db.tododb.find({},{ "_id": 0, "close":0})
        if (flask.request.args.get('top')):
            k = int(flask.request.args.get('top'))
        if k > 0:
            _items = db.tododb.find({},{ "_id": 0, "close":0}).sort('open',pymongo.ASCENDING)
            for item in _items:
                items.append(item)
                count += 1
                if k == count:
                    break
        else:
            items = [item for item in _items]
        item = str(re.sub('\[','',str(items)))
        item = str(re.sub(']','',str(item)))
        item = str(re.sub('}','',str(item)))
        item = str(re.sub('{','',str(item)))
        response = make_response(item)
        return response
api.add_resource(listOpencsv, '/listOpenOnly/csv')

class listClosecsv(Resource): #Works as intended
    def get(self):
        items =[]
        k = 0
        count = 0
        _items = db.tododb.find({},{ "_id": 0, "open":0})
        if (flask.request.args.get('top')):
            k = int(flask.request.args.get('top'))
        if k > 0:
            _items = db.tododb.find({},{ "_id": 0, "open":0}).sort('close',pymongo.ASCENDING)
            for item in _items:
                items.append(item)
                count += 1
                if k == count:
                    break
        else:
            items = [item for item in _items]
        item = str(re.sub('\[','',str(items)))
        item = str(re.sub(']','',str(item)))
        item = str(re.sub('}','',str(item)))
        item = str(re.sub('{','',str(item)))
        response = make_response(item)
        return response
api.add_resource(listClosecsv, '/listCloseOnly/csv')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
